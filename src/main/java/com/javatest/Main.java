package com.javatest;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.javatest.dao.BooksListImpl;
import com.javatest.entity.Book;


import io.jsondb.JsonDBTemplate;

public class Main {
	
	

	
	public static void main(String args[]){  
		
		BooksListImpl booksListImpl = new BooksListImpl();

		//Actual location on disk for database files, process should have read-write permissions to this folder
		String dbFilesLocation = System.getProperty("user.dir");

		//Java package name where POJO's are present
		String baseScanPackage ="com.javatest";
		JsonDBTemplate jsonDBTemplate = new JsonDBTemplate(dbFilesLocation, baseScanPackage, null);
		
		if ((new File("/Users/heer/microservices/java-bookstoe-cli", "instances.json").exists())) {
	
		} else {
			jsonDBTemplate.createCollection(Book.class);	
		}

		Scanner sc=new Scanner(System.in);  
		     
		   System.out.println("Welcome to awesome book store!"
				+"\n Please select following option"
		   		+ "\n To add book, please enter 1"
		   		+ "\n To search book, please enter 2"
		   		+ "\n To delete book, please enter 3"
		   		+ "\n To Exit, please enter 4 "); 
		for (; ;) {
			int selection= sc.nextInt();
	
			//BooksListImpl booksListImpl= new BooksListImpl();
			
			//Optionally a Cipher object if you need Encryption
			//ICipher cipher = new DefaultAESCBCCipher("1r8+24pibarAWgS85/Heeg==");

			
			 if (selection==1) {
					
				Date dNow = new Date();
					SimpleDateFormat ft = new SimpleDateFormat("MMddhhmmssSSS");
					Book book = new Book();		
			
				book.setId(Long.valueOf(ft.format(dNow)) );
				System.out.println("Please enter below information to add a book");
				 System.out.println("Enter Name: ");
				 sc.next();
				 String title=sc.nextLine();
				 book.setTitle(title);
				 
				 System.out.println("Enter Author: ");
				 String author=sc.nextLine();
				 book.setAuthor(author);
				 
				 System.out.println("Enter Price: ");
				 String price=sc.next();
				 //BigDecimal p= new BigDecimal(12.78);
				 book.setPrice(Double.valueOf(price));
				 System.out.println(book.getPrice());
				
				 System.out.println("Enter Quantity: ");
				 int qty=sc.nextInt();
			
				 //jsonDBTemplate.upsert(book);
				 booksListImpl.add(book, qty);
				 System.out.println("Book added is " + book.toString()); 
				 
				 System.out.println("Thanks!!"
							+"\n Please select following option"
					   		+ "\n To add book, please enter 1"
					   		+ "\n To search book, please enter 2"
					   		+ "\n To delete book, please enter 3"
					   		+ "\n To Exit, please enter 4 "); 
				 
			}
			 
			 if (selection==2) {
				// String jxQuery = String.format("/.[id='%s']", "714083004386");
				 System.out.println("Enter All to list all books or"
				 		+ "\n Enter Book Name or Author Name: ");
				 //sc.next();
				 String bookName=sc.next();
			try {
				 Book[] books= booksListImpl.list(bookName);
				 int numberOfBookSearched=books.length;
				
				for (int i = 0; i < numberOfBookSearched; i++) {
					System.out.println("Book name :" + books[i].getTitle() +"Author name :" +books[i].getAuthor());
				}
			

			} catch (Exception e) {
					
					}
				 System.out.println("Thanks!!"
							+"\n Please select following option"
					   		+ "\n To add book, please enter 1"
					   		+ "\n To search book, please enter 2"
					   		+ "\n To delete book, please enter 3"
					   		+ "\n To Exit, please enter 4 ");
			}
			
			   if (selection==4) {
				  
				   System.out.println("Thanks for using this application. Have a good one!!");
				   sc.close();
				   break;
			}
			   
		 
			 }  
		}



}
