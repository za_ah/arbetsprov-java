package com.javatest.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.javatest.entity.Book;

import io.jsondb.JsonDBTemplate;


public class BooksListImpl implements BookList{
	
	

	//Actual location on disk for database files, process should have read-write permissions to this folder
	String dbFilesLocation = System.getProperty("user.dir");

	//Java package name where POJO's are present
	String baseScanPackage ="com.javatest";
	JsonDBTemplate jsonDBTemplate = new JsonDBTemplate(dbFilesLocation, baseScanPackage, null);
	
	public Book[] list(String searchString) {
		Book[] bookslist=new Book[1000];
		int i=0;	
		 String findbyTitle = String.format("/.[title='%s']", searchString);
		 String findbyAuthor = String.format("/.[author='%s']", searchString);
			
		 List<Book> instances = jsonDBTemplate.find(findbyTitle, Book.class);
		 List<Book> instancesAuthor = jsonDBTemplate.find(findbyAuthor, Book.class);
		 Book bk= new Book();
		 System.out.println("Search started...");
		 if(searchString.equalsIgnoreCase("ALL")){
			 System.out.println("Searching all books");
			 List<Book> allInstances = jsonDBTemplate.findAll(Book.class);
			 if (allInstances.size()>0) {
					
					for (Book book : allInstances) {
						bk.setTitle(book.getTitle());
						bk.setAuthor(book.getAuthor());
						bk.setQuantity(book.getQuantity());
						System.out.println("TITLE of the book :" + bk.getTitle());
						bookslist[i]=bk;
						i++;
						
					}
					
		 }
			 return bookslist;
		 }
		 if (instances.size()>0) {
		
			for (Book book : instances) {
				bk.setTitle(book.getTitle());
				bk.setAuthor(book.getAuthor());
				bk.setQuantity(book.getQuantity());
				System.out.println("TITLE of the book :" + bk.getTitle());
				bookslist[i]=bk;
				i++;
			}
			System.out.println("No of books by title " + instances.size() );
			return bookslist;
		 }
			if (instancesAuthor.size()>0) {
			
				for (Book book : instancesAuthor) {
					bk.setTitle(book.getTitle());
					bk.setAuthor(book.getAuthor());
					bk.setQuantity(book.getQuantity());
					System.out.println("Author of the book :" + bk.getTitle());
					bookslist[i]=bk;
					i++;
				}
				System.out.println("No of books by author " + instancesAuthor.size());
				return bookslist;	
		}
		
		
		return null; 
		
	}

	public boolean add(Book book, int quantity) {
		boolean bookAdded=false;
		String jxQuery = String.format("/.[title='%s']", book.getTitle());
		List<Book>  booksbBefore = jsonDBTemplate.find(jxQuery, Book.class);
		
			book.setQuantity(quantity);
			jsonDBTemplate.upsert(book);
			List<Book>  booksAfter= jsonDBTemplate.find(jxQuery, Book.class);
		if (booksAfter.size()>0) {
			bookAdded=true;
		}
		return bookAdded;
	}

	public int[] buy(Book... books) {
		// TODO Auto-generated method stub
		return null;
	}

}

